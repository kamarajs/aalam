package com.Flipkart.Testcase;



import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.Flipkart.page.BrowserInvoke;
import com.Utilities.DataInputProvider;
import com.Utilities.DriverInit;




public class BrowserInvokeTestcase extends DriverInit {

	public static String dataSheetName,sheetName;

	@DataProvider (name = "fetchData")
	public String[][] getData() throws IOException {
		String[][] sheet = DataInputProvider.getSheet(dataSheetName, sheetName);
		return sheet;
	}
	
	@BeforeTest
	public void setData() {
		/*testCaseName = "Browser Invoked";
		testCaseDescription = "Browser Invoked";*/
		category = "Smoke";
		author = "Kamaraj";
		dataSheetName = "Supportdata";
		sheetName = "Browser";
	}



	@Test(dataProvider = "fetchData")
	public void Browserset(String browser,String URL) throws InterruptedException {
	    String messagealert ="";
		new BrowserInvoke().Browser(browser).applicationurl(URL);
		Thread.sleep(3000);
		try {
			if (!(driver.findElements(By.xpath("//div[@class=\"alert alert-success\"]")).size() > 0)) {
			} else {
				messagealert = driver.findElement(By.xpath("//div[@class=\"alert alert-success\"]")).getText().replaceAll("\\s", " ").substring(1).trim();
				if ( messagealert != null )
				{
					handlePass(messagealert);
				}
			}
		}
		catch (AssertionError e) {
			reportStep(messagealert, "pass");
			takeSnap(messagealert);
		}
	
	try {
		if (!(driver.findElements(By.xpath("//div[@class=\"alert alert-danger\"]")).size() > 0)) {
		} else {
			messagealert = driver.findElement(By.xpath("//div[@class=\"alert alert-danger\"]")).getText().replaceAll("\\s", " ").substring(1).trim();
			if ( messagealert != null )
			{
				handleErrors(messagealert);
			}
		}
	}
	catch (AssertionError e) {
		reportStep(messagealert, "fail");
		takeSnap(messagealert);
	}
	
	try {
		if (!(driver.findElements(By.xpath("//div[@class=\"field-validation-error\"]")).size() > 0)) {
		} else {
			messagealert = driver.findElement(By.xpath("//div[@class=\"field-validation-error\"]")).getText().replaceAll("\\s", " ").substring(1).trim();
			if ( messagealert != null )
			{
				handleErrors(messagealert);
			}
		}
	}
	catch (AssertionError e) {
		reportStep(messagealert, "warning");
		takeSnap(messagealert);

	}
}

	public void handleErrors(String errorMsg) throws InterruptedException
	{
		System.out.println(errorMsg);
		reportStep(errorMsg, "fail");
		takeSnap(errorMsg);
	}

	public void handleWarns(String warnMsg) throws InterruptedException
	{
		System.out.println(warnMsg);
		reportStep(warnMsg, "Warning");
		takeSnap(warnMsg);

	}

	public void handlePass(String passMsg) throws InterruptedException
	{
		System.out.println(passMsg);
		reportStep(passMsg, "Pass");
		takeSnap(passMsg);
	}

	public void takeSnap(String Str) {
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyHHmmss");
		Date date = new Date();
		String Projectenddate = formatter.format(date);
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./Resources/snaps/" + Str + Projectenddate + ".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			System.err.println("IOException");
		}
	}


}
