package com.Utilities;


import java.text.SimpleDateFormat;
import java.util.Date;


import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;

import org.testng.annotations.BeforeSuite;


import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class HtmlReporter {
	public static ExtentReports extent;
	public static ExtentTest test;
	public static String testCaseName, testCaseDescription, author, category;
	
	@BeforeSuite // One time execution 
	public void startResult() {
		String timeStamp = new SimpleDateFormat("dd.MM.yyyy.HH.mm.ss").format(new Date());
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("./Resources/reports/" + "result" + timeStamp + ".html");
		htmlReporter.setAppendExisting(false);
		ExtentHtmlReporter Reporter = new ExtentHtmlReporter("./Resources/Jenkinsreports/" + "result" + ".html");
		Reporter.setAppendExisting(false);
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		extent.attachReporter(Reporter);

	}
	
	@BeforeClass
	public void startTestCase() {
		//test = extent.createTest(testCaseName, testCaseDescription);
		//test = extent.createTest((this.getClass().getSimpleName() +" - "+  method.getName()),method.getName());
		test = extent.createTest((this.getClass().getSimpleName()));
		//test = extent.createTest((method.getName()),method.getName());
		test.assignAuthor(author);
		test.assignCategory(category);
	
	}
	
	
	public void reportStep(String desc,String status) {
		if (status.equalsIgnoreCase("pass")) {
			test.pass(desc);
		}if (status.equalsIgnoreCase("fail")) {
			test.fail(desc);
		}if (status.equalsIgnoreCase("warning")) {
			test.warning(desc);
		}
	}
	
	
	
	@AfterSuite
	public void stopResult(){		
		extent.flush();
	}
}
