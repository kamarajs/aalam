package com.Utilities;


import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverInit extends HtmlReporter  {
	public int i = 1;
	public static RemoteWebDriver driver;
	public void startApp(String browser ) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./Resources/driver/chromedriver.exe");
				//WebDriverManager.chromedriver().setup();
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {
				//WebDriverManager.firefoxdriver().setup();
				System.setProperty("webdriver.gecko.driver", "./Resources/driver/geckodriver.exe");
				driver = new FirefoxDriver();
				
			}  else if (browser.equalsIgnoreCase("Edge")) {
				//WebDriverManager.firefoxdriver().setup();
				System.setProperty("webdriver.edge.driver", "./Resources/driver/MicrosoftWebDriver.exe");
				driver = new EdgeDriver();
			} 
			
			
			
			driver.manage().window().maximize();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			reportStep("The browser " + browser + " launched successfully", "pass");
		} catch (WebDriverException  e)  {
			reportStep("The browser " + browser + " could not be launched", "fail");
		}
	}		
		public String getCurrentUrl() {
			String bReturn = "";
			try {
				bReturn = driver.getCurrentUrl();
				reportStep("The URL is: " + bReturn + " ", "pass");
			} catch (WebDriverException e) {
				reportStep("Unknown Exception Occured While fetching Title", "fail");
			}
			return bReturn;
		}
		
		
	}
