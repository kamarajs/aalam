package com.Utilities;

import java.io.FileInputStream;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataInputProvider{

	public static String[][] getSheet(String filename,String sheetname) {

		String[][] data = null;
		try {
			FileInputStream fis = new FileInputStream("./Resources/data/"+filename+".xlsx");
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			XSSFSheet sheet = workbook.getSheet(sheetname);	

			// get the number of rows
			int rowCount = sheet.getLastRowNum();


			// get the number of columns
			int columnCount = sheet.getRow(0).getLastCellNum();
			System.out.println("This file having: " + rowCount + "rows" + "," + columnCount + "columns");
			data = new String[rowCount][columnCount];

			// loop through the rows
			for(int i=1; i <rowCount+1; i++){
				try {
					//XSSFRow row = sheet.getRow(i);
					for(int j=0; j <columnCount; j++){ // loop through the columns
						try {
							String cellValue = "";
							try{
								DataFormatter formatter = new DataFormatter();
								//cellValue = row.getCell(j).getStringCellValue();
								cellValue = formatter.formatCellValue(sheet.getRow(i).getCell(j));
								
								}catch(NullPointerException e){

							}
							
							data[i-1][j]  = cellValue; // add to the data array
						} catch (Exception e) {
							e.printStackTrace();
						}				
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			fis.close();
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}




}
