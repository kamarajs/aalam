package com.Flipkart.page;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


import com.Utilities.DriverInit;

public class ProductSearch extends DriverInit {

	public ProductSearch() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "(//button)[2]")
	WebElement Loginclose;

	@FindBy(name = "q")
	WebElement ProjectSearch;

	@FindBy(xpath = "//button[@class=\"vh79eN\"]")
	WebElement Searchsubmit;

	@FindBy(xpath = "//img[@alt=\"Pigeon Playboy Sports Sipper Steel Water Bottle 700 ml Water Bottle\"]")
	WebElement SelectProduct;

	@FindBy(xpath = "(//ul[@class=\"row\"]/li)[1]")
	WebElement Addcart;

	@FindBy(xpath = "//button[@class=\"_2AkmmA iwYpF9 _7UHT_c\"]")
	WebElement palaceorder;

	@FindBy(xpath = "(//input)[1]")
	WebElement Loginname;

	@FindBy(xpath = "(//Input)[2]")
	WebElement Loginpassword;

	@FindBy(xpath = "//button[@class=\"_2AkmmA _1poQZq _7UHT_c\"]")
	WebElement continues;

	@FindBy(xpath = "//button[@class=\"_2AkmmA _1poQZq _7UHT_c\"]")
	WebElement continuesaa;

	@FindBy(xpath = " //button[contains(text(),'Deliver Here')]")
	WebElement Deliver;

	@FindBy(xpath = "//*[text()=\"Remove\"]")
	WebElement Remove;
	
	public void takeSnap(String Str) {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./Resources/snaps/" + Str + ".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (java.io.IOException e) {
			System.err.println("IOException");
		}
	}

	public ProductSearch search(String Product) throws InterruptedException {
		try {
		if(driver.findElement(By.xpath("//input[@type='password']")).isDisplayed()) { 
			Loginclose.click(); 
			
			           } 
			   
			  }catch(Throwable t) { 
			   
			   System.out.println("Lightbox not displayed"); 
			   
			  } 
		
		ProjectSearch.sendKeys(Product);
		takeSnap("Sumbit");
		Searchsubmit.submit();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		SelectProduct.click();
		takeSnap("SelectProduct");
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(0));
		driver.close();
		driver.switchTo().window(tabs2.get(1));
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Addcart.click();
		Thread.sleep(3000);
		palaceorder.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return this;
	}
	
	public ProductSearch login(String Username, String Password) throws InterruptedException {
	takeSnap("Login");
	Loginname.sendKeys(Username);
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	continues.click(); 
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	Loginpassword.sendKeys(Password);
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	continuesaa.click(); 
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	takeSnap("Delivery");
	Deliver.click();
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	Remove.click();
	takeSnap("Remove");
	return this;
	}
}
