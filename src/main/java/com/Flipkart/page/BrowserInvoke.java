package com.Flipkart.page;

import org.openqa.selenium.support.PageFactory;

import com.Utilities.DriverInit;



public class BrowserInvoke extends DriverInit {
	
	public BrowserInvoke applicationurl(String URL) {
		driver.get(URL);
		return this;
	}
	
	// Browser Invoke Method
	public BrowserInvoke Browser(String browser) {
		PageFactory.initElements(driver, this);
		startApp(browser);
		reportStep(browser + "Browser Invoked", "pass");
		driver.manage().deleteAllCookies();
		return this;
	}


}
